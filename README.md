# atom-setup

## Packages
- [emmet](https://emmet.io)
- [beautify](https://github.com/Glavin001/atom-beautify)
- [LaTeXTools for Atom](https://atom.io/users/msiniscalchi)
